CREATE TABLE IF NOT EXISTS ecommerce.users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    national_id VARCHAR(10) UNIQUE NOT NULL,
    full_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    phone_number CHAR(11) NOT NULL,
    credit_card CHAR(16),
    birth_date DATE NOT NULL,
    password VARCHAR(255) NOT NULL,
    gender TINYINT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS ecommerce.newsletters (
    user_id INT NOT NULL PRIMARY KEY,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.coupons (
    id INT AUTO_INCREMENT PRIMARY KEY,
    coupon_text TEXT NOT NULL,
    user_id INT NOT NULL,
    discount INT NOT NULL,
    coupon_type INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP,
    expired_at DATE,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.states (
    id INT AUTO_INCREMENT PRIMARY KEY,
    state VARCHAR(255) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS ecommerce.cities (
    id INT AUTO_INCREMENT PRIMARY KEY,
    state_id INT NOT NULL,
    city VARCHAR(255) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (state_id)
        REFERENCES states (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.addresses (
    id INT AUTO_INCREMENT PRIMARY KEY,
    state_id INT NOT NULL,
    city_id INT NOT NULL,
    user_id INT NOT NULL,
    location POINT NOT NULL,
    full_address VARCHAR(1000) NOT NULL,
    postal_code CHAR(10) NOT NULL,
    phone CHAR(20) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (state_id)
        REFERENCES states (id)
        ON DELETE CASCADE,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE,
    FOREIGN KEY (city_id)
        REFERENCES cities (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.brands (
    id INT AUTO_INCREMENT PRIMARY KEY,
    persian_name VARCHAR(255) NOT NULL,
    english_name VARCHAR(255) NOT NULL,
    image VARCHAR(255) NOT NULL,
    rate FLOAT DEFAULT 0,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS ecommerce.categories (
    id INT AUTO_INCREMENT PRIMARY KEY,
    parent_id INT,
    persian_name VARCHAR(255) NOT NULL,
    english_name VARCHAR(255) NOT NULL,
    slug VARCHAR(255),
    is_active TINYINT DEFAULT 1,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (parent_id)
        REFERENCES categories(id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.commentable (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    commentable_id INT NOT NULL,
    commentable_type VARCHAR(255) NOT NULL,
    title VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    verfied_at TIMESTAMP,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.payments (
    id INT AUTO_INCREMENT PRIMARY KEY,
    coupons_id INT,
    item_count INT NOT NULL,
    total_price INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (coupons_id)
        REFERENCES coupons (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.shipments (
    id INT AUTO_INCREMENT PRIMARY KEY,
    address_id INT NOT NULL,
    shipment_status TINYINT NOT NULL,
    shipment_type TINYINT NOT NULL,
    shipment_price INT NOT NULL,
    delivery_date DATE NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (address_id)
        REFERENCES addresses (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.orders (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    payment_id INT NOT NULL,
    shipment_id INT NOT NULL,
    item_count INT NOT NULL,
    total_price INT NOT NULL,
    order_status TINYINT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE,
    FOREIGN KEY (payment_id)
        REFERENCES payments (id)
        ON DELETE CASCADE,
    FOREIGN KEY (shipment_id)
        REFERENCES shipments (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.products (
    id INT AUTO_INCREMENT PRIMARY KEY,
    brand_id INT NOT NULL,
    persian_name VARCHAR(255) NOT NULL,
    english_name VARCHAR(255) NOT NULL,
    is_active TINYINT DEFAULT 1,
    slug VARCHAR(255),
    description TEXT not null,
    point INT,
    rate FLOAT,
    video VARCHAR(255),
    available TINYINT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (brand_id)
        REFERENCES brands (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.images (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT NOT NULL,
    image varchar(255) not null,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (product_id)
        REFERENCES products (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.attribute_key (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title varchar(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS ecommerce.attribute_value (
    id INT AUTO_INCREMENT PRIMARY KEY,
    value varchar(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS ecommerce.product_attributes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    attribute_key_id INT NOT NULL,
    attribute_value_id INT NOT NULL,
    product_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (product_id)
        REFERENCES products (id)
        ON DELETE CASCADE,
    FOREIGN KEY (attribute_key_id)
        REFERENCES attribute_key (id)
        ON DELETE CASCADE,
    FOREIGN KEY (attribute_value_id)
        REFERENCES attribute_value (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.categories_products (
    id INT AUTO_INCREMENT PRIMARY KEY,
    category_id INT NOT NULL,
    product_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (product_id)
        REFERENCES products (id)
        ON DELETE CASCADE,
    FOREIGN KEY (category_id)
        REFERENCES categories (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.vendors (
    id INT AUTO_INCREMENT PRIMARY KEY,
    persian_name VARCHAR(255) NOT NULL,
    english_name VARCHAR(255) NOT NULL,
    image VARCHAR(255) NOT NULL,
    description TEXT,
    active TINYINT DEFAULT 1,
    rate FLOAT,
    address_id INT NOT NULL,
    percent FLOAT NOT NULL,
    badge TINYINT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    verified_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (address_id)
        REFERENCES addresses (id)
        ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS ecommerce.vendor_product (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT NOT NULL,
    vendor_id INT NOT NULL,
    count INT NOT NULL,
    price FLOAT NOT NULL,
    active TINYINT DEFAULT 1,
    discount FLOAT,
    discount_type VARCHAR(30),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (product_id)
        REFERENCES products (id)
        ON DELETE CASCADE,
    FOREIGN KEY (vendor_id)
        REFERENCES vendors (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.order_item (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT NOT NULL,
    order_id INT NOT NULL,
    item_count INT DEFAULT 1,
    total_price FLOAT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (product_id)
        REFERENCES vendor_product (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.pointable (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    pointable_id INT NOT NULL,
    pointable_type VARCHAR(255) not null,
    point INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.wishlists (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    product_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE,
    FOREIGN KEY (product_id)
        REFERENCES products (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.notifications (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    product_id INT NOT NULL,
    availabe TINYINT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE,
    FOREIGN KEY (product_id)
        REFERENCES products (id)
        ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS ecommerce.history(
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    product_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE,
    FOREIGN KEY (product_id)
        REFERENCES products (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ecommerce.shoppingcart(
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    product_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE,
    FOREIGN KEY (product_id)
        REFERENCES vendor_product (id)
        ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS ecommerce.likeable (
    id INT AUTO_INCREMENT PRIMARY KEY,
    likeable_id INT NOT NULL,
    likeable_type VARCHAR(50) NOT NULL,
    user_id INT NOT NULL,
    value INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE
);


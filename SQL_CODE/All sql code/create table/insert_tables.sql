-- Insert Data into states table 
INSERT INTO ecommerce.states (state) VALUES 
('Isfahan'),
('Tehran'), 
('Mashad'),
('Zanjan');

-- Insert Data into cities table 
INSERT INTO ecommerce.cities (state_id, city) VALUES 
(1,'کاشان'),
(1,'گلپایگان'), 
(1,'اصفهان'),
(1,'نطنز'),
(2,'تهران'),
(2,'شهریار'), 
(2,'فیروزکوه'),
(2,'دماوند');


-- Insert Data into users table 
INSERT INTO ecommerce.users (national_id ,full_name, email, phone_number, credit_card, birth_date, password, gender) VALUES 
('4576532457','محمد رضا باقری', 'mrezabaqery77@gmail.com', '09411245487', '5741657812458495', '1399/01/01', '123456789', 1),
('7896321457','جواد جوادی', 'javad@gmail.com', '09411145789', '5741657812458495', '1399/02/01', '123456789', 1),
('0106532951','اصغر اصغری', 'asqar@gmail.com', '09113215487', '5741657812458495', '1399/03/01', '123456789', 1);

-- Insert Data into addresses table 
INSERT INTO ecommerce.addresses (state_id,city_id, user_id, location,full_address,postal_code, phone) VALUES 
(1,1, 1, Point(1, 3), 'اصفهان, کاشان', '5465879562', '05165847542'),
(1,2, 1, Point(1, 3), 'اصفهان, کاشان', '5465879562', '05165847542'),
(1,3, 1, Point(1, 3), 'اصفهان, کاشان', '5465879562', '05165847542');

-- Insert Data into newsletter table 
INSERT INTO ecommerce.newsletters (user_id) VALUES 
(1),(2),(3);

-- Insert Data into coupons table 
INSERT INTO ecommerce.coupons (coupon_text, user_id, discount, coupon_type, expired_at) VALUES 
('MissuTFFR-5tu9',1, 25,0,"1399/06/06"),
('MissuTFFR-98y4g',2, 25,0,"1399/06/06"),
('MissuTFFR-5tu31419',3, 25,0, "1399/06/06");

-- Insert into attributeKeys table
insert into ecommerce.attribute_key (title) values 
("رنگ"),
("جنس"),
("اندازه"),
("وزن"),
("مدل پردازنده");

-- Insert into AttributeValues table
insert into ecommerce.attribute_value (value) values
("زرد"),
("مشکی"),
("سیاه"),
("چوبی"),
("2 متر"),
("3 کیلو گرم"),
("corei 7");

-- Insert into brands table
insert into ecommerce.brands (persian_name, english_name, image, rate) values
("ایسوس", "Asus", "/public/image/asus.png", 0),
("لنوو", "lenovo", "/public/image/lenovo.png", 9.7),
("ام اس آی", "MSI", "/public/image/Msi.png", 5.6),
("ایسر", "Acer", "/public/image/acer.png", 7.5),
("سامسونگ", "Samsong", "/public/image/Samsong.png", 10.0);

-- Insert into products table
insert into ecommerce.products (persian_name, english_name, is_active, brand_id, description, point, rate, video, available) values
("لپتاپ مدل k556U", "Asus K556U", 1, 1, "A laptop", 10, 0, "public/video/k566u.mp4", 1),
("لپتاپ مدل X556U", "Asus X556U", 1, 1, "A laptop", 20, 5.7, "public/video/X566u.mp4", 1),
("لپتاپ مدل GV62", "Msi GV62", 0, 3, "A laptop", 30, 9.7, "public/video/GV62.mp4", 1);

-- Insert into images table
insert into ecommerce.images (product_id, image) values 
(1, "public/image/k556u.png"),
(1, "public/image/k556u_1.png"),
(1, "public/image/k556u_2.png"),
(1, "public/image/k556u_3.png"),
(1, "public/image/k556u_4.png"),
(2, "public/image/X556u_1.png"),
(2, "public/image/X556u_2.png"),
(2, "public/image/X556u_3.png"),
(2, "public/image/X556u_4.png"),
(3, "public/image/Msi_1.png"),
(3, "public/image/Msi_2.png"),
(3, "public/image/Msi_3.png"),
(3, "public/image/Msi_4.png");

-- Insert into productAttribute table
insert into ecommerce.product_attributes (attribute_key_id, attribute_value_id, product_id) values
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(1, 1, 2),
(2, 2, 2),
(3, 3, 2),
(1, 1, 3),
(2, 2, 3),
(3, 3, 3);

-- Insert into categories table
insert into ecommerce.categories (persian_name, english_name, is_active) values
("دستگاه های برقی","Electronical Devices", 1),
("لوازم کامپیوتر","Computer Accessories", 1),
("لپتاپ","Laptop", 1);

-- Insert into categories product
insert into ecommerce.categories_products (product_id, category_id) values
(1, 2),
(2, 2),
(3, 2),
(1, 1),
(2, 1),
(3, 1);

-- Insert into vendors table
insert into ecommerce.vendors (persian_name, english_name, image, description, active, rate, address_id, percent, badge) values
("فروشگاه لوازم کامپیوتر تسکو", "Tsco", "public/vendors/tsco.png", "A vendor", 1, 9.6, 1, 5, 0);

-- Insert into vendor product table
insert into ecommerce.vendor_product (product_id, vendor_id, count, price, active, discount, discount_type) values
(1, 1, 10, 9000000,  1, 0, 0),
(2, 1, 20, 19000000, 1, 10, 1),
(3, 1, 30, 29000000, 1, 20, 1);

-- Insert commentable history
insert into ecommerce.commentable (user_id, commentable_id,  commentable_type, title, description) values
(1, 1,"Class/Product", "comment Title", "Comment Description"),
(1, 2,"Class/Product", "comment Title", "Comment Description"),
(1, 3,"Class/Product", "comment Title", "Comment Description"),
(1, 1,"Class/Vendor", "Comment Title", "Comment Description");

-- Insert into history
insert into ecommerce.history (user_id, product_id) values
(1,1),
(1,2),
(1,3),
(2,1),
(2,2),
(2,3);

    -- Insert into likeable
    insert into ecommerce.likeable (likeable_id, likeable_type, user_id, value) values
    (1, "Class/Product", 1, 1),
(2, "Class/Product", 1, 1),
(3, "Class/Product", 1, 1),
(1, "Class/Product", 2, 1),
(2, "Class/Product", 2, 1),
(3, "Class/Product", 2, 1),
(1, "Class/Product", 3, 1),
(1, "Class/Vendor", 1, 1),
(1, "Class/Vendor", 2, 1),
(1, "Class/Vendor", 3, 1);


-- Insert into shopping cart 
insert into ecommerce.shoppingcart (user_id, product_id) values
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3);

-- Insert into pointable
insert into ecommerce.pointable (user_id, pointable_id, pointable_type, point) values
(1, 1, "Class/Product", 10),
(2, 2, "Class/Product", 20),
(3, 3, "Class/Product", 30);

-- Insert into payment
insert into ecommerce.payments (coupons_id, item_count, total_price) values
(null, 1, 1000000),
(null, 2,2000000),
(null, 1, 300000);

-- Insert into shipment
insert into ecommerce.shipments (address_id, shipment_status, shipment_type, shipment_price, delivery_date) values
(1, 0, 0, 150000, "1399/01/01"),
(2, 0, 0, 150000, "1399/01/01"),
(3, 0, 0, 150000, "1399/01/01");


-- Insert into order
insert into ecommerce.orders (user_id, payment_id, shipment_id, item_count, total_price, order_status) values
(1, 1, 1, 1, 1000000,0),
(2, 2, 2, 2, 2000000,0),
(3, 3, 3, 1, 300000, 0);

-- Insert into order item
insert into ecommerce.order_item (product_id, order_id, item_count, total_price) values
(1, 1,  1, 1000000),
(2, 2,  2, 2000000),
(3, 3,  1, 300000);
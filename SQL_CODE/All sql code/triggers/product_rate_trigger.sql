DELIMITER $$

CREATE TRIGGER product_rate
AFTER INSERT
ON ecommerce.likeable FOR EACH ROW
BEGIN
    DECLARE like_value INT;
        DECLARE likeableId INT;

SELECT 
    ecommerce.likeable.likeable_id, SUM(ecommerce.likeable.value)
INTO likeableId, like_value FROM
    ecommerce.likeable
WHERE
    ecommerce.likeable.likeable_id = new.likeable_id
        AND ecommerce.likeable.likeable_type = 'Class/Product'
GROUP BY ecommerce.likeable.likeable_id;
    
	UPDATE ecommerce.products 
SET 
    ecommerce.products.rate = like_value
WHERE
    ecommerce.products.id = new.likeable_id;
END$$

DELIMITER ;
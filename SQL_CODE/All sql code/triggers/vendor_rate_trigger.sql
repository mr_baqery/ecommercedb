DELIMITER $$

CREATE TRIGGER vendor_rate
AFTER INSERT
ON ecommerce.likeable FOR EACH ROW
BEGIN
    DECLARE like_value INT;
        DECLARE likeable_id INT;

SELECT 
    ecommerce.likeable.likeable_id, SUM(ecommerce.likeable.value)
INTO likeable_id,like_value FROM
    ecommerce.likeable
WHERE
    ecommerce.likeable.likeable_id = new.likeable_id
        AND ecommerce.likeable.likeable_type = 'Class/Vendor'
GROUP BY ecommerce.likeable.likeable_id;
    
	UPDATE ecommerce.vendors 
SET 
    ecommerce.vendors.rate = like_value
WHERE
    ecommerce.vendors.id = new.likeable_id;
END$$

DELIMITER ;
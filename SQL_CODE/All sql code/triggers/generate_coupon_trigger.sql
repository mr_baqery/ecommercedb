DELIMITER $$

CREATE TRIGGER generate_coupon
AFTER INSERT
ON ecommerce.pointable FOR EACH ROW
BEGIN
    DECLARE point_sum INT;
    DECLARE user_id INT;
	SELECT 
    ecommerce.pointable.user_id, SUM(ecommerce.pointable.point)
INTO user_id, point_sum FROM
    ecommerce.pointable
WHERE
    ecommerce.pointable.user_id = new.user_id
GROUP BY ecommerce.pointable.user_id;

	IF point_sum >= 1000 THEN
		INSERT INTO ecommerce.coupons (coupon_text, user_id, discount, coupon_type, expired_at) VALUES 
		('MissuTFFKKKR-5tu9',new.user_id, 25, 0, DATE_ADD( CURRENT_DATE, INTERVAL 10 DAY));
        
DELETE FROM ecommerce.pointable 
WHERE
    ecommerce.pointable.user_id = new.user_id;
        
    END IF;
    
END$$

DELIMITER ;
DELIMITER $$

CREATE TRIGGER after_comment_insert
AFTER INSERT
ON ecommerce.commentable FOR EACH ROW
BEGIN
	INSERT INTO ecommerce.pointable (user_id, pointable_id, pointable_type, point)
	VALUES  (new.user_id, new.commentable_id, new.commentable_type, 10);
END$$

DELIMITER ;
DELIMITER $$

CREATE TRIGGER vendor_billing
AFTER INSERT
ON ecommerce.orders FOR EACH ROW
BEGIN
    DECLARE order_count INT;
        DECLARE vendor_id INT;

SELECT 
    ecommerce.vendors.id, COUNT(ecommerce.orders.id)
INTO vendor_id,order_count FROM
    ecommerce.order_item
        INNER JOIN
    ecommerce.orders ON ecommerce.order_item.order_id = ecommerce.orders.id
        INNER JOIN
    ecommerce.vendor_product ON ecommerce.order_item.product_id = ecommerce.vendor_product.id
        INNER JOIN
    ecommerce.vendors ON ecommerce.vendors.id = ecommerce.vendor_product.vendor_id
WHERE
    ecommerce.orders.order_status = 0
GROUP BY ecommerce.vendors.id;
    
    IF order_count >= 4 THEN
		UPDATE ecommerce.vendors 
        set ecommerce.vendors.percent = 4 WHERE ecommerce.vendors.id = vendor_id;
    END IF;
    IF order_count >= 2000 THEN
		UPDATE ecommerce.vendors 
        set ecommerce.vendors.percent = 3.5 WHERE ecommerce.vendors.id = vendor_id;
    END IF;
    IF order_count >= 4000 THEN
		UPDATE ecommerce.vendors 
        set ecommerce.vendors.percent = 3.25 WHERE ecommerce.vendors.id = vendor_id;
    END IF;
    IF order_count >= 8000 THEN
		UPDATE ecommerce.vendors 
        set ecommerce.vendors.percent = 3 WHERE ecommerce.vendors.id = vendor_id;
    END IF;
    IF order_count >= 16000 THEN
		UPDATE ecommerce.vendors 
        set ecommerce.vendors.percent = 2.75 WHERE ecommerce.vendors.id = vendor_id;
    END IF;
    IF order_count >= 32000 THEN
		UPDATE ecommerce.vendors 
        set ecommerce.vendors.percent = 2.5 WHERE ecommerce.vendors.id = vendor_id;
    END IF;
END$$

DELIMITER ;

-- drop trigger vendor_billing;

DELIMITER $$
CREATE PROCEDURE createCouponsList (
	IN userId int,
	INOUT couponsList varchar(4000)

)
BEGIN
	declare finished integer default 0;
    declare coupon varchar(255) default "";
    
    declare curCopouns
		cursor for 
			select coupon_text from ecommerce.coupons inner join ecommerce.users on ecommerce.users.id = ecommerce.coupons.user_id where ecommerce.users.id = userId;
            
	declare continue handler
    for not found set finished = 1;
    
    open curCopouns;
    
    getCoupons : loop
		fetch curCopouns into coupon;
        if finished = 1 then
			leave getCoupons;
		end if;
        
        set	couponsList =  concat(coupon,";",couponsList);
	end loop getCoupons;
    close curCopouns;
    
END$$
DELIMITER ;


SET @emailList = ""; 
CALL createCouponsList(@emailList); 
SELECT @emailList;
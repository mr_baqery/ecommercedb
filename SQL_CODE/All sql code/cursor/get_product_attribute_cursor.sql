DELIMITER $$
CREATE PROCEDURE getProductAttribute(
	in productId integer,
    inout productAttribute varchar(8000)
)
Begin
	declare title varchar(255);
    declare value varchar(255);
    declare pAttri varchar(255);
    declare finished integer default 0;
    
    declare product cursor for
		select ecommerce.attribute_key.title,
				ecommerce.attribute_value.value
		from ecommerce.product_attributes inner join ecommerce.attribute_key on ecommerce.product_attributes.attribute_key_id = ecommerce.attribute_key.id
        inner join ecommerce.attribute_value on ecommerce.product_attributes.attribute_value_id = ecommerce.attribute_value.id
			where ecommerce.product_attributes.product_id = productId;
	
    declare continue handler
    for not found set finished = 1;
    open product;
    
    getAttribute : loop
		fetch product into title, value;
        if finished = 1 then
			leave getAttribute;
		end if;
        
        set	pAttri = concat(title,";",value);
        Set productAttribute = concat(pAttri,"|",productAttribute);
	end loop getAttribute;
    close product;
    
End $$
DELIMITER ;
DELIMITER $$

CREATE FUNCTION `get_user_point` (user_id int)
RETURNS INTEGER
DETERMINISTIC
BEGIN
    DECLARE point_sum INT;
    DECLARE userid INT;

	SELECT 
    ecommerce.pointable.user_id, SUM(ecommerce.pointable.point)
INTO userid, point_sum FROM
    ecommerce.pointable
WHERE
    ecommerce.pointable.user_id = user_id
GROUP BY ecommerce.pointable.user_id; 
RETURN point_sum;
END$$
DELIMITER ;

-- select id , get_user_point(id) as "point_sum" from users;
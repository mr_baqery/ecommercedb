DELIMITER $$

CREATE FUNCTION BrandPopularity(
	brand_id int
) 
RETURNS VARCHAR(20)
DETERMINISTIC
BEGIN
    DECLARE brandLevel VARCHAR(20);
    DECLARE rate float;
	
    select ecommerce.brands.rate into rate from ecommerce.brands
		where ecommerce.brands.id = brand_id;
        
    IF rate < 500 THEN
		SET brandLevel = 'BRONZE';
    end if;

    IF (rate >= 500 AND 
			rate <= 10000) THEN
        SET brandLevel = 'SILVER';
    end if;

    IF rate >= 10000 THEN
        SET brandLevel = 'GOLD';
    END IF;
	-- return the customer level
	RETURN (brandLevel);
END$$
DELIMITER ;

-- Select id, BrandPopularity(id) as 'brandLevel' from brands;

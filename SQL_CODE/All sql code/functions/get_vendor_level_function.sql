DELIMITER $$

CREATE FUNCTION VendorLevel(
	vendor_id int
) 
RETURNS VARCHAR(20)
DETERMINISTIC
BEGIN
    DECLARE vendorLevel VARCHAR(20);
    DECLARE rate float;
	
    select ecommerce.vendors.rate into rate from ecommerce.vendors
		where ecommerce.vendors.id = vendor_id;
        

         
    IF rate < 500 THEN
		SET vendorLevel = 'BRONZE';
    end if;

    IF (rate >= 500 AND 
			rate <= 10000) THEN
        SET vendorLevel = 'SILVER';
    end if;

    IF rate >= 10000 THEN
        SET vendorLevel = 'GOLD';
    END IF;

	-- return the customer level
	RETURN (vendorLevel);
END$$
DELIMITER ;

-- select id , VendorLevel(id) as 'vendorLevel' from vendors;
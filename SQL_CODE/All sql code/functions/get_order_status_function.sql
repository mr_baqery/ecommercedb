DELIMITER $$

CREATE FUNCTION get_order_status(
	order_status tinyInt
) 
RETURNS  VARCHAR(20) 
DETERMINISTIC
BEGIN
    
    DECLARE status_var VARCHAR(20);
        
    IF order_status = 0 THEN
		SET status_var = "پرداخت شده";
    ELSEIF (order_status = 1 ) THEN
        SET status_var = 'پرداخت نشده';
    ELSEIF order_status = 2 THEN
        SET status_var = 'لغو شده';
    END IF;
	-- return the customer level
	RETURN (status_var);
END$$
DELIMITER ;

-- select id , get_order_status(order_status) as "status_var" from orders;
SELECT 
    email
FROM
    ecommerce.newsletters
        inner JOIN
    ecommerce.users ON ecommerce.newsletters.user_id = ecommerce.users.id;
    

SELECT 
    email, coupon_text, expired_at
FROM
    ecommerce.coupons
        INNER JOIN
    ecommerce.users ON ecommerce.coupons.user_id = ecommerce.users.id
WHERE
    ecommerce.coupons.expired_at >= CURRENT_DATE()
;

Select
	ecommerce.users.email,
    ecommerce.addresses.full_address,
    ecommerce.addresses.postal_code,
    ecommerce.addresses.location
From
	ecommerce.users
		inner join
	ecommerce.addresses on ecommerce.users.id = ecommerce.addresses.user_id
		inner join
    ecommerce.states on ecommerce.states.id = ecommerce.addresses.state_id
    where ecommerce.states.state = "Isfahan";
    

Select
	ecommerce.users.email,
    ecommerce.commentable.commentable_id,
    ecommerce.commentable.commentable_type
From
	ecommerce.users
		inner join
	ecommerce.commentable on ecommerce.users.id = ecommerce.commentable.user_id
    where ecommerce.commentable.verified_at between '2000-07-05' and '2020-02-15';


SELECT 
    ecommerce.users.email,
    ecommerce.commentable.commentable_id
FROM
    ecommerce.users
        INNER JOIN
    ecommerce.commentable ON ecommerce.users.id = ecommerce.commentable.user_id
WHERE
    ecommerce.commentable.commentable_type = 'Class/Product'
        AND ecommerce.users.email = 'mrezabaqery77@gmail.com'
        AND ecommerce.commentable.commentable_id IN (SELECT 
            ecommerce.order_item.product_id
        FROM
            ecommerce.users
                INNER JOIN
            ecommerce.orders ON ecommerce.users.id = ecommerce.orders.user_id
                INNER JOIN
            ecommerce.order_item ON ecommerce.orders.id = ecommerce.order_item.order_id
				where ecommerce.users.email = 'mrezabaqery77@gmail.com'
            );
            
            
SELECT 
    COUNT(ecommerce.products.id),
    ecommerce.categories.persian_name
FROM
    ecommerce.products
        INNER JOIN
    ecommerce.categories_products ON ecommerce.categories_products.product_id = ecommerce.products.id
        INNER JOIN
    ecommerce.categories ON ecommerce.categories.id = ecommerce.categories_products.category_id
GROUP BY ecommerce.categories.persian_name;


SELECT 
    SUM(ecommerce.pointable.point),
    ecommerce.pointable.pointable_type
FROM
    ecommerce.pointable
WHERE
    ecommerce.pointable.user_id = 1
GROUP BY ecommerce.pointable.pointable_type;



           
SELECT 
    ecommerce.products.persian_name,
    ecommerce.products.english_name,
    ecommerce.attribute_key.title,
    ecommerce.attribute_value.value
FROM
    ecommerce.products
        INNER JOIN
    ecommerce.product_attributes ON ecommerce.product_attributes.product_id = ecommerce.products.id
        INNER JOIN
    ecommerce.attribute_key ON ecommerce.product_attributes.attribute_key_id = ecommerce.attribute_key.id
        INNER JOIN
    ecommerce.attribute_value ON ecommerce.product_attributes.attribute_value_id = ecommerce.attribute_value.id;



           
SELECT 
    ecommerce.vendor_product.product_id,
    ecommerce.products.persian_name,
    ecommerce.products.english_name,
    ecommerce.vendors.persian_name,
    ecommerce.vendor_product.count
FROM
    ecommerce.vendors
        INNER JOIN
    ecommerce.vendor_product ON ecommerce.vendors.id = ecommerce.vendor_product.vendor_id
        INNER JOIN
    ecommerce.products ON ecommerce.products.id = ecommerce.vendor_product.product_id
GROUP BY ecommerce.vendor_product.product_id;


SELECT 
    ecommerce.users.id,
    ecommerce.users.email,
    ecommerce.users.full_name,
    AVG(ecommerce.orders.total_price)
FROM
    ecommerce.users
        INNER JOIN
    ecommerce.orders ON ecommerce.users.id = ecommerce.orders.user_id
GROUP BY ecommerce.users.id;



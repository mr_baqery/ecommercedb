SELECT 
    *
FROM
    ecommerce.users;

select persian_name from ecommerce.users inner join ecommerce.orders on ecommerce.users.id = ecommerce.orders.id 
inner join ecommerce.order_item on ecommerce.order_item.order_id = ecommerce.orders.id
inner join ecommerce.vendor_product on ecommerce.vendor_product.product_id = ecommerce.order_item.product_id
inner join ecommerce.products on ecommerce.products.id = ecommerce.vendor_product.product_id;


select ecommerce.orders.id  from ecommerce.users inner join ecommerce.orders on ecommerce.users.id = ecommerce.orders.id 
	where ecommerce.orders.order_status = 0;
    
SELECT 
    commentable_id
FROM
    ecommerce.commentable
        INNER JOIN
    ecommerce.users ON ecommerce.users.id = ecommerce.commentable.user_id
WHERE
    ecommerce.commentable.commentable_type = "Class/Product";
    


SELECT 
    product_id
FROM
    ecommerce.shoppingcart
        INNER JOIN
    ecommerce.users ON ecommerce.users.id = ecommerce.shoppingcart.user_id
WHERE
    ecommerce.shoppingcart.user_id = 1;
    

SELECT 
    likeable_id
FROM
    ecommerce.likeable
        INNER JOIN
    ecommerce.users ON ecommerce.users.id = ecommerce.likeable.user_id
WHERE
    ecommerce.likeable.likeable_type = "Class/Product";
    


SELECT 
    likeable_id
FROM
    ecommerce.likeable
        INNER JOIN
    ecommerce.users ON ecommerce.users.id = ecommerce.likeable.user_id
WHERE
    ecommerce.likeable.likeable_type = "Class/Comment";
    
    

SELECT 
    *
FROM
    ecommerce.products inner join ecommerce.categories_products on ecommerce.categories_products.product_id = ecommerce.products.id
    inner join ecommerce.categories on ecommerce.categories_products.product_id = ecommerce.categories.id
    group by ecommerce.categories.id;
    

SELECT 
    *
FROM
    ecommerce.products inner join ecommerce.brands on ecommerce.brands.id = ecommerce.products.brand_id
    group by ecommerce.brands.id;
    
SELECT 
    *
FROM
    ecommerce.products inner join ecommerce.vendor_product on ecommerce.vendor_product.product_id = ecommerce.products.id
	where ecommerce.vendor_product.count > 0;
    
    

SELECT 
    *
FROM
    ecommerce.products inner join ecommerce.vendor_product on ecommerce.vendor_product.product_id = ecommerce.products.id
	order by ecommerce.vendor_product.price desc ;

SELECT 
    *
FROM
    ecommerce.products order by ecommerce.products.created_at asc;



SELECT 
    *
FROM
    ecommerce.products
        INNER JOIN
    ecommerce.commentable ON ecommerce.commentable.commentable_id = ecommerce.products.id
WHERE
    ecommerce.commentable.commentable_type = 'Class/Product';
    



SELECT 
    ecommerce.commentable.commentable_id,
    COUNT(ecommerce.users.id)
FROM
    ecommerce.users
        INNER JOIN
    ecommerce.commentable ON ecommerce.commentable.user_id = ecommerce.users.id
WHERE
    ecommerce.commentable.commentable_type = 'Class/Product'
GROUP BY ecommerce.commentable.commentable_id;



SELECT 
    ecommerce.likeable.likeable_id,
    COUNT(ecommerce.users.id)
FROM
    ecommerce.users
        INNER JOIN
    ecommerce.likeable ON ecommerce.likeable.user_id = ecommerce.users.id
WHERE
    ecommerce.likeable.likeable_type = 'Class/Product'
GROUP BY ecommerce.likeable.likeable_id;



SELECT 
    *
FROM
    ecommerce.vendors
        INNER JOIN
    ecommerce.vendor_product ON ecommerce.vendors.id = ecommerce.vendor_product.vendor_id;
    

SELECT 
    *
FROM
    ecommerce.vendors;
    

SELECT 
    *
FROM
    ecommerce.users
        INNER JOIN
    ecommerce.orders ON ecommerce.orders.user_id = ecommerce.users.id
        INNER JOIN
    ecommerce.payments ON ecommerce.payments.id = ecommerce.orders.payment_id
		where ecommerce.orders.order_status = 0;
    

SELECT 
    *
FROM
    ecommerce.users
        INNER JOIN
    ecommerce.orders ON ecommerce.orders.user_id = ecommerce.users.id
        INNER JOIN
    ecommerce.shipments ON ecommerce.shipments.id = ecommerce.orders.shipment_id
		where ecommerce.orders.order_status = 0;
    


SELECT 
    *
FROM
    ecommerce.users
        INNER JOIN
    ecommerce.likeable ON ecommerce.likeable.user_id = ecommerce.users.id
WHERE
    ecommerce.likeable.likeable_type = 'Class/Vendor';
    
    
select * from ecommerce.categories;

select * from ecommerce.brands;

SELECT 
    *
FROM
    ecommerce.users
        INNER JOIN
    ecommerce.commentable ON ecommerce.commentable.user_id = ecommerce.users.id
WHERE
    ecommerce.commentable.user_id = 1;
    


SELECT 
    *
FROM
    ecommerce.orders
        INNER JOIN
    ecommerce.order_item ON ecommerce.orders.id = ecommerce.order_item.order_id
        INNER JOIN
    ecommerce.vendor_product ON ecommerce.vendor_product.id = ecommerce.order_item.product_id
        INNER JOIN
    ecommerce.products ON ecommerce.vendor_product.product_id = ecommerce.products.id;


SELECT 
    *
FROM
	ecommerce.payments
		inner join
    ecommerce.orders on ecommerce.payments.id = ecommerce.orders.payment_id
        INNER JOIN
    ecommerce.order_item ON ecommerce.orders.id = ecommerce.order_item.order_id
        INNER JOIN
    ecommerce.vendor_product ON ecommerce.vendor_product.id = ecommerce.order_item.product_id
        INNER JOIN
    ecommerce.products ON ecommerce.vendor_product.product_id = ecommerce.products.id;


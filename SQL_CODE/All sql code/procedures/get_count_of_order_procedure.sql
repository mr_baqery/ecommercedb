DELIMITER $$

CREATE PROCEDURE GetTotalOrder()
BEGIN
	DECLARE totalOrder INT DEFAULT 0;
    
SELECT 
    COUNT(*)
INTO totalOrder FROM
    ecommerce.orders;
    
SELECT totalOrder;
END$$

DELIMITER ;
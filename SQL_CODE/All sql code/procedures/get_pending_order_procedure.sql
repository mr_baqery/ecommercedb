DELIMITER $$

CREATE PROCEDURE shipmentStatus(
    IN  shipmentID INT,
    OUT shipmentState       VARCHAR(50)
    )
BEGIN
    DECLARE statusVar tinyInt;

SELECT 
    shipment_status
INTO statusVar FROM
    ecommerce.shipments
WHERE
    ecommerce.shipments.id = shipmentID;
	
    CASE statusVar
		WHEN  0 THEN
		   SET shipmentState = 'ارسال شده';
		WHEN 1 THEN
		   SET shipmentState = 'در حال ارسال';
		ELSE
		   SET shipmentState = 'ارسال نشده';
	END CASE;
SELECT (shipmentState);
END$$

DELIMITER ;


-- SET @shipment = ""; 

-- call shipmentStatus(1, @shipment);
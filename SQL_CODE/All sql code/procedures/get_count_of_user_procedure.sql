DELIMITER $$

CREATE PROCEDURE usersFromCity(IN stateId int)
BEGIN
	DECLARE userCount INT DEFAULT 0;
    DECLARE state_id INT DEFAULT 0;

	SELECT 
    ecommerce.addresses.state_id, COUNT(ecommerce.users.id)
INTO state_id, userCount FROM
    ecommerce.users
        INNER JOIN
    ecommerce.addresses ON ecommerce.addresses.user_id = ecommerce.users.id where ecommerce.addresses.state_id = stateId;
    
SELECT userCount;
END$$

DELIMITER ;
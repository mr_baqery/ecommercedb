DELIMITER $$

CREATE PROCEDURE AddProdcutQuantity(
    IN  productId INT,
    in vendor_id INT,
    in quantity int
    )
BEGIN
update ecommerce.vendor_product 
set ecommerce.vendor_product.count = ecommerce.vendor_product.count + quantity 
where ecommerce.vendor_product.product_id = productId and ecommerce.vendor_product.vendor_id = vendor_id;
END$$

DELIMITER ;